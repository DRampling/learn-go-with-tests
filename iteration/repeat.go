package iteration

// Repeat returns a concatenated string of the provided character repeated by
// provided count of times:
func Repeat(character string, count int) string {
	var repeated string
	for i := 0; i < count; i++ {
		repeated += character
	}
	return repeated
}
