package iteration

import (
	"fmt"
	"testing"
)

func ExampleRepeat() {
	repeatedCharacters := Repeat("e", 5)
	fmt.Println(repeatedCharacters)
	// Output: "eeeee"
}

func TestRepeat(t *testing.T) {
	repeated := Repeat("a", 10)
	expected := "aaaaa"

	if repeated != expected {
		t.Errorf("expected %q but got %q", expected, repeated)
	}
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 10)
	}
}
