package main

const (
	// ErrNotFound returns the copy for not found
	ErrNotFound = DictionaryErr("could not find the word you were looking for")
	// ErrWordExists returns the copy for a word already existing
	ErrWordExists = DictionaryErr("cannot add word because it already exists")
	// ErrWordDoesNotExist returns the copy not updating the non-existing word
	ErrWordDoesNotExist = DictionaryErr("cannot update word because it does not exist")
)

// DictionaryErr are reusable dictionary errors
type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}

// Dictionary stores key value pairs of strings
type Dictionary map[string]string

// Search looks for a word in the dictionary
func (d Dictionary) Search(word string) (string, error) {
	definition, ok := d[word]
	if !ok {
		return "", ErrNotFound
	}

	return definition, nil
}

// Add adds a word to the dictionary
func (d Dictionary) Add(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		d[word] = definition
	case nil:
		return ErrWordExists
	default:
		return err
	}

	return nil
}

// Update updates a word in the dictionary
func (d Dictionary) Update(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		return ErrWordDoesNotExist
	case nil:
		d[word] = definition
	default:
		return err
	}

	return nil
}

// Delete removes a word from the dictionary
func (d Dictionary) Delete(word string) {
	delete(d, word)
}
