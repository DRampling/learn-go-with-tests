package main

import (
	"errors"
	"fmt"
)

// ErrInsufficientFunds returns the copy for insufficient funds
var ErrInsufficientFunds = errors.New("cannot withdraw, insufficient funds")

// Bitcoin is a special currency
type Bitcoin int

func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

// Wallet stores botcoins
type Wallet struct {
	balance Bitcoin
}

// Deposit accepts Bitcoins to increment wallet count
func (w *Wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

// Balance returns the number of stored Bitcoins
func (w *Wallet) Balance() Bitcoin {
	return w.balance
}

// Withdraw accepts Bitcoins to decrease the wallet count
func (w *Wallet) Withdraw(amount Bitcoin) error {

	if amount > w.balance {
		return ErrInsufficientFunds
	}

	w.balance -= amount
	return nil
}
